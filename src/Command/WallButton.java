/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author emil
 */
public class WallButton {
    Command cmds;
    public WallButton(Command cmd){
        this.cmds = cmd;
    }
    public void push() {
        cmds.execute();
    }
}

package FactoryMethod;

public class Main extends Alustus {

    public static void main(String[] args) {
        AterioivaOtus opettaja = CreateOpettaja();
        opettaja.aterioi();
        AterioivaOtus oppilas = CreateOppilas();
        oppilas.aterioi();
        AterioivaOtus opo = CreateOpo();
        opo.aterioi();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryMethod;

/**
 *
 * @author emil
 */
public abstract class Alustus {
    public static Opettaja CreateOpettaja(){
        return new Opettaja();
    }
    public static Oppilas CreateOppilas(){
        return new Oppilas();
    }
    public static Opo CreateOpo(){
        return new Opo();
    }
    
}

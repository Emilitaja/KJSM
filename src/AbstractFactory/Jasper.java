/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author emil
 */
public class Jasper {
String paita;
String housut;
String hattu;
String kengät;
    public Jasper() {
    }
    public void pue(ClothingFactory tehdas){
        paita = tehdas.createPaita();
        housut = tehdas.createHousut();
        hattu = tehdas.createHattu();
        kengät = tehdas.createKengät();
        laitaPäälle();
    }
    public void laitaPäälle(){
        System.out.println(paita);
        System.out.println(housut);
        System.out.println(hattu);
        System.out.println(kengät);
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author emil
 */
public class AdidasFactory implements ClothingFactory{
    public String createPaita(){
        return "Adidaksen Paita";
    }
    public String createHousut(){
        return "Adidaksen Housut";
    }
    public String createHattu(){
        return "Adidaksen Hattu";
    }
    public String createKengät(){
        return "Adidaksen Kengät";
    }    
}

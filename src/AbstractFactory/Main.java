/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args) {
        Jasper jasper = new Jasper();
        ClothingFactory tehdas = new BossFactory();
        jasper.pue(tehdas);
        tehdas = new AdidasFactory();
        jasper.pue(tehdas);
    }
}

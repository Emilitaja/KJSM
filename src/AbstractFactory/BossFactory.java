/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author emil
 */
public class BossFactory implements ClothingFactory{
    public String createPaita(){
        return "Bossin Paita";
    }
    public String createHousut(){
        return "Bossin Housut";
    }
    public String createHattu(){
        return "Bossin Hattu";
    }
    public String createKengät(){
        return "Bossin Kengät";
    }
}

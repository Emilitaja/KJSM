/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author emil
 */
public interface ClothingFactory {
    public String createPaita();
    public String createHousut();
    public String createHattu();
    public String createKengät();
}

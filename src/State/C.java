/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author emil
 */
public class C extends State{
    public void changeState(Monsteri monsteri){
        monsteri.setState(new A());
    }
    public void attackA() { System.out.println( "Ensimmäisen asteen hyökkäys C"); }
    public void attackB() { System.out.println( "Toisen asteen hyökkäys C"); }
    public void attackC() { System.out.println( "kolmannen asteen hyökkäys C"); }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author emil
 */
public class Monsteri {
    private State state;
    public Monsteri(State state){
        this.state = state;
    }
    public void attackA()  { state.attackA();}
    public void attackB() { state.attackB();}
    public void attackC() { state.attackC();}

    public void changeState() {
        state.changeState(this);
    }
    protected void setState(State state){
        this.state = state;
    }
}

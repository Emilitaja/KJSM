/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        Monsteri ok  = new Monsteri(new A());
        ok.attackA();
        ok.changeState();
        ok.attackB();
        ok.attackC();
        ok.changeState();
        ok.attackA();
        ok.attackB();
        ok.attackC();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author emil
 */
public class WellRested extends State{
    Pelihahmo pelihahmo;
    double multiplier = 1.20;
    public void changeState(Pelihahmo pelihahmo){
        pelihahmo.setState(new Normal());
    }

    public double getMultiplier() {
        return multiplier;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author emil
 */
public class Pelihahmo {
    public String name;
    public double exp = 0;
    private State state;

    public Pelihahmo() {
    }

    public Pelihahmo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getExp() {
        return exp;
    }

    public State getState() {
        return state;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExp(double exp) {
        this.exp = exp;
    }

    public void setState(State state) {
        this.state = state;
    }
    public void changeState() {
        state.changeState(this);
    }
    public void status(){
        System.out.println("nimi: "+getName()+" EXP: "+getExp());
    }

    public void gainExp(double i) {
        setExp(getExp() +( i * getState().getMultiplier()));
    }
}

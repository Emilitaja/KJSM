/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        Pelihahmo pelaaja1 = new Pelihahmo("pekka");
        Pelihahmo pelaaja2 = new Pelihahmo("liisa");
        Pelihahmo pelaaja3 = new Pelihahmo("ville");
        Pelihahmo pelaaja4 = new Pelihahmo("jaska");
        Pelihahmo pelaaja5 = new Pelihahmo("hermanni");
        pelaaja1.setState(new Normal());
        pelaaja2.setState(new WellRested());
        pelaaja3.setState(new Tired());
        pelaaja4.setState(new Normal());
        pelaaja5.setState(new WellRested());
        EXP expa = new EXP() {};
        
        expa.visit(pelaaja1);
        expa.visit(pelaaja1);
        expa.visit(pelaaja2);
        expa.visit(pelaaja3);
        expa.visit(pelaaja3);
        expa.visit(pelaaja3);
        expa.visit(pelaaja4);
        expa.visit(pelaaja5);
        expa.visit(pelaaja5);
        pelaaja5.changeState();
        expa.visit(pelaaja5);
        pelaaja5.changeState();
        expa.visit(pelaaja5);
        
        pelaaja1.status();
        pelaaja2.status();
        pelaaja3.status();
        pelaaja4.status();
        pelaaja5.status();
    }
}
 
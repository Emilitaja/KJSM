/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author emil
 */
public class Normal extends State{
    Pelihahmo pelihahmo;
    double multiplier = 1.00;
    public void changeState(Pelihahmo pelihahmo){
        pelihahmo.setState(new Tired());
    }
    public double getMultiplier() {
        return multiplier;
    }
}

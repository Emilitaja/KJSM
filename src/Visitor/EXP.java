/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author emil
 */
public abstract class EXP implements Visitor{
    
    public void visit(Pelihahmo pelihahmo){
        pelihahmo.gainExp(100.0);
        System.out.println("Pelaaja sai 10xp lisää! Nykyinen xp määrä on: "+pelihahmo.getExp());
    }
}

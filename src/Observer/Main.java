/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args) throws Exception{
        ClockTimer timer = new ClockTimer(12, 0, 0);
        DigitalClock clock = new DigitalClock(timer);
        timer.addObserver(clock);
        
        while(true){
            timer.tick();
            Thread.sleep(10);
        }
    }
}

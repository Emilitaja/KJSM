/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author emil
 */
public class DigitalClock implements Observer{
    
    private ClockTimer timer;
    
    public DigitalClock(ClockTimer ct){
        timer = ct;
    }
    
    public void update(Observable obs, Object obj){
        if (obs==timer){
            draw((int[]) obj);
        }
    }
    private void draw(int[] lista){
        int hour = lista[2];
        int min = lista[1];
        int sec = lista[0];
        System.out.println(hour + ":" + min + ":" + sec);
    }
}

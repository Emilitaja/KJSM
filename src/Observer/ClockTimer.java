/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Observer;

import java.util.Observable;

/**
 *
 * @author emil
 */
public class ClockTimer extends Observable{
    int hour;
    int min;
    int sec;

    ClockTimer(int i, int i0, int i1) {
        hour=i;
        min=i0;
        sec=i1;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getSec() {
        return sec;
    }
    void tick(){
        sec++;
        if (sec == 60){
            sec=0;
            min++;
            if (min == 60){
                min=0;
                hour++;
                if(hour == 24){
                    hour=0;
                }
            }
        }
        setChanged();
        int[] lista = new int[]{sec, min, hour};
        notifyObservers(lista);
    }
}

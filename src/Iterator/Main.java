/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args) {
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("eka");
		lista.add("toka");
		lista.add("kolmas");
		lista.add("neljäs");
                
                System.out.println("iterator pyörii yksin");
                Iterator<String> listaIterator = lista.iterator();
		while (listaIterator.hasNext()) {
			System.out.println(listaIterator.next());
		}
                
                System.out.println("2 iteraattoria pyörii erillään toisistaan");
                Iteratora1(lista);
                Iteratora2(lista);
                
                System.out.println("2 iteraattoria pyörittää samaa iteraattoria");
                Iterator<String> listaIteratorB = lista.iterator();
                Iteratorb1(listaIteratorB);
                
                System.out.println("kokoelmaan tehdään muutoksia läpikäynnin aikana");
                Iteratorc(lista);
                
                
    }

    private static void Iteratora1(ArrayList lista) {
        Iterator<String> listaIterator1 = lista.iterator();
	while (listaIterator1.hasNext()) {
            System.out.println(listaIterator1.next());
	}
    }

    private static void Iteratora2(ArrayList lista) {
        Iterator<String> listaIterator2 = lista.iterator();
	while (listaIterator2.hasNext()) {
            System.out.println(listaIterator2.next());
	}
    }

    private static void Iteratorb1(Iterator<String> listaIteratorB) {
        while (listaIteratorB.hasNext()) {
            System.out.println(listaIteratorB.next());
            Iteratorb2(listaIteratorB);
	}
    }

    private static void Iteratorb2(Iterator<String> listaIteratorB) {
        while (listaIteratorB.hasNext()) {
            System.out.println(listaIteratorB.next());
            Iteratorb1(listaIteratorB);
	}
    }

    private static void Iteratorc(ArrayList<String> lista) {
        Iterator<String> listaIteratorC = lista.iterator();
        int x = 0;
        while (listaIteratorC.hasNext()) {
            x++;
            System.out.println(listaIteratorC.next());
            if(x==2){
                lista.add("terve");
                //listaIteratorC = lista.iterator();
                System.out.println("listaan lisättiin 'terve' ja iterator päivitettiin");
            }
	}
    }
}

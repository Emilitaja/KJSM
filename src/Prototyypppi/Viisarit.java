/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prototyypppi;

/**
 *
 * @author emil
 */
public class Viisarit implements Cloneable{
    int s;
    int m;
    int h;

    public Viisarit() {
    }
    
    public Viisarit(int s, int m, int h) {
        this.s = s;
        this.m = m;
        this.h = h;
    }

    public int getS() {
        return s;
    }

    public int getM() {
        return m;
    }

    public int getH() {
        return h;
    }

    public void setS(int s) {
        this.s = s;
    }

    public void setM(int m) {
        this.m = m;
    }

    public void setH(int h) {
        this.h = h;
    }

    void tick() {
        s++;
        if (s==60){
            s=0;
            m++;
            if (m==60){
                m=0;
                h++;
                if (h==24){
                    h=0;
                }
            }
        }
    }
    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}

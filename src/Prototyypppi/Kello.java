/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prototyypppi;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author emil
 */
public class Kello implements Cloneable{
    Viisarit viisarit;
    boolean jatka = true;

    public Kello(Viisarit viisarit) {
        this.viisarit = viisarit;
    }

    public Kello() {
    }

    public Viisarit getViisarit() {
        return viisarit;
    }

    public void setViisarit(Viisarit viisarit) {
        this.viisarit = viisarit;
    }
    
    public void run(){
        while (jatka()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Kello.class.getName()).log(Level.SEVERE, null, ex);
            }
            viisarit.tick();
            showTime();}
    }
    public void showTime(){
        System.out.println(viisarit.getH()+":"+viisarit.getM()+":"+viisarit.getS());
    }
    public Kello clone() {
        Kello a = null;
        try {
        a = (Kello)super.clone();
        a.viisarit = (Viisarit)viisarit.clone();
        } catch (CloneNotSupportedException e) {}
        return a;
    }

    private boolean jatka() {
        if (viisarit.getS()==59 && jatka==true){
            jatka=false;
            return jatka;
        }else if (jatka==false){
            jatka=true;
            return jatka;
        }
        return jatka;
    }
}

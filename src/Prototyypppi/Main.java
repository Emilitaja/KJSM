/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prototyypppi;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        Viisarit viisarit = new Viisarit(20, 58, 23);
        Kello kello = new Kello(viisarit);
        kello.run();
        System.out.println("ohjelmaa on juostu aloitetaan kopionti");
        Kello uusikello = kello.clone();
        uusikello.run();
        System.out.println("kopiota on juostu nyt vähän aikaa");
        System.out.println("alkuperäinen:");
        kello.showTime();
        System.out.println("kopio:");
        uusikello.showTime();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        
        //luodaan puhdas string
        String puhdas = "hello world";
        System.out.println("alkuperäinen string: " + puhdas);
        Password salis = new SimplePassword();
        
        //sotketaan string
        Password salattuSalasana = new DecoratedPassword(salis);
        salattuSalasana.write(puhdas);
        System.out.println("sotkettu: " + salattuSalasana.read());
        
        //putsataan string
        Password puhdasSalasana = new DecodePassword(salis);
        puhdasSalasana.write(salattuSalasana.read());
        System.out.println("aikaisempi sotku putsattu: " + puhdasSalasana.read());
        
        Password Madness = new MadnessPassword(new MadnessPassword(salis));
        Madness.write("hello my honey");
        System.out.println("hacker search: "+salis.read());
        System.out.println(Madness.read());
        
        //putsataan sotkettu string
        String likainen = "hsffujoht";
        puhdasSalasana = new DecodePassword(salis);
        puhdasSalasana.write(likainen);
        System.out.println("sotkuinen string " + likainen + " putsattu: " + puhdasSalasana.read());
    }
}

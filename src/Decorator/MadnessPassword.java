/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author emil
 */
public class MadnessPassword extends PasswordDecorator{
    
    public MadnessPassword(Password password) {
        super(password);
    }
    @Override
    public void write(String Arvo){
        char[] lista = Arvo.toCharArray();
        char[] ab = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (int i = 0; i < lista.length; i++) {
            boolean notChecked = true;
            for (int k = 0; k < ab.length; k++){
                if (lista[i] == ab[k] && notChecked){
                    lista[i]=ab[k-1];
                    notChecked = false;
                }
            }
        }
        String muokattava = new String(lista);
        super.write(muokattava);
    }
    @Override
    public String read(){
        String Arvo=super.read();
        char[] lista = Arvo.toCharArray();
        char[] ab = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (int i = 0; i < lista.length; i++) {
            boolean notChecked = true;
            for (int k = 0; k < ab.length; k++){
                if (lista[i] == ab[k] && notChecked){
                    lista[i]=ab[k+1];
                    notChecked = false;
                }
            }
        }
        String muokattava = new String(lista);
        return muokattava;
    }
    public void mess(String Arvo){
        char[] lista = Arvo.toCharArray();
        char[] ab = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (int i = 0; i < lista.length; i++) {
            boolean notChecked = true;
            for (int k = 0; k < ab.length; k++){
                if (lista[i] == ab[k] && notChecked){
                    lista[i]=ab[k-1];
                    notChecked = false;
                }
            }
        }
        String muokattava = new String(lista);
        super.write(muokattava);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author emil
 */
public abstract class PasswordDecorator implements Password{
    Password password;
    byte[] b;
    public PasswordDecorator (Password password){
        this.password = password;
    }
    public void write(String Arvo){
        password.write(Arvo);
    }
    public String read(){
        return password.read();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package COR;

/**
 *
 * @author emil
 */
public class Request {
    private int nosto;
    private boolean arvio = false;

    public Request(int nosto) {
        this.nosto = nosto;
    }

    public Request() {
    }

    public void setNosto(int nosto) {
        this.nosto = nosto;
    }

    public int getNosto() {
        return nosto;
    }

    public boolean isArvio() {
        return arvio;
    }

    public void setArvio(boolean arvio) {
        this.arvio = arvio;
    }
    
}

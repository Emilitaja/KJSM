/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package COR;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        Lähiesimies le = new Lähiesimies();
        Yksikönpäälikkö yp = new Yksikönpäälikkö();
        Toimitusjohtaja ceo = new Toimitusjohtaja();
        le.setSuccessor(yp);
        yp.setSuccessor(ceo);
        Request request = new Request();
        request.setNosto(10);
        le.handle(request);
        request.setNosto(3);
        le.handle(request);
        request.setNosto(1);
        le.handle(request);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package COR;

/**
 *
 * @author emil
 */
class Toimitusjohtaja extends Successor{
    protected Request request;
    protected Successor successor;

    public void handle(Request request) {
        this.request = request;
        if (request.getNosto() > 5){
            request.setArvio(true);
            System.out.println("hyväksytty request tj");
        }else{
            successor.handle(request);
        }
    }

    public void setSuccessor(Successor yp) {
        this.successor = yp;
    }
}

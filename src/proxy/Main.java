/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.ArrayList;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        final Image image1 = new ProxyImage("HiRes_10MB_Photo1");
        final Image image2 = new ProxyImage("HiRes_10MB_Photo2");
        final Image image3 = new ProxyImage("HiRes_10MB_Photo3");
        
        ArrayList<Image> valokuvakansio = new ArrayList<Image>();
        valokuvakansio.add(image1);
        valokuvakansio.add(image2);
        valokuvakansio.add(new ProxyImage("HiRes_10MB_Photo3"));
        tulosta(valokuvakansio);

        image1.displayImage(); // loading necessary
        image1.displayImage(); // loading unnecessary
        image2.displayImage(); // loading necessary
        image2.displayImage(); // loading unnecessary
        image1.displayImage(); // loading unnecessary
        
        selaa(valokuvakansio);
    }

    private static void tulosta(ArrayList<Image> valokuvakansio) {
        System.out.println("tulostus alkaa");
        for(int i=0;i<valokuvakansio.size(); i++){
            valokuvakansio.get(i).showData();
        }
        System.out.println("tulostus loppuu");
    }

    private static void selaa(ArrayList<Image> valokuvakansio) {
        System.out.println("selaa");
        for(int i=0; i<valokuvakansio.size();i++){
            valokuvakansio.get(i).displayImage();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package template;

import java.util.Scanner;

/**
 *
 * @author emil
 */
public class KSP extends Game{
    int kivi = 0;
    int sakset = 1;
    int paperi = 2;
    
    int eka = 1;
    int ekanValinta;
    int toka = 2;
    int tokanValinta;
    
    boolean pelijatkuu = true;
    
    int arvo=0;
    int valinta;
    
    int pelaaja = 1;
    
    public static Scanner in = new Scanner(System.in);
    
    public void init(){
        kivi = 0;
        sakset = 1;
        paperi = 2;

        eka = 1;
        ekanValinta = 3;
        toka = 2;
        tokanValinta = 3;

        pelijatkuu = true;

        arvo=6;
        pelaaja=1;
    }
    public void play(int pelaaja){
        arvo = 5;
        while (!(arvo == 0 || arvo == 1 || arvo == 2)){
            System.out.println("1. kivi? 2. sakset? 3. paperi?");
            valinta = in.nextInt();
            if (valinta == 1){arvo = 0;}
            else if (valinta == 2){arvo = 1;}
            else if (valinta == 3){arvo = 2;}
        }
        if (pelaaja == 1){ekanValinta = arvo;}
        else {
            tokanValinta = arvo;
            if (ekanValinta+1 == tokanValinta ||
                ekanValinta == 2 && tokanValinta == 0){
                endGame();
                winner(eka);
            }else if(ekanValinta == tokanValinta){
                System.out.println("tasan! uusi peli");
                playing();
            }
            else{
                endGame();
                winner(toka);
            }
        }
        
    }
    public void endGame(){pelijatkuu = false;}
    public void winner(int player){
        System.out.println("Pelaaja "+player+" voitti!");
    }
    public void playing(){
        init();
        while(pelijatkuu){
            play(pelaaja);
            if (pelaaja==1){pelaaja=2;}
            else{pelaaja=1;}
        }
    };
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author emil
 */
public class Client {

 	/**
* to get raw materials
*/
    public static void main(String[] args) {
        StoreKeeper keeper = new StoreKeeper();
        FinishedGoods finishedMaterialGoods = keeper.getFinishedGoods();
        System.out.println(finishedMaterialGoods.getTuote());
    }
}// End of class

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author emil
 */
public class StoreKeeper {
    /**
    * The finished goods are asked for and
    * are returned
    *
    * @return finished goods
    */
    public FinishedGoods getFinishedGoods() {
        FinishedGoodsStore store = new FinishedGoodsStore();
        FinishedGoods finishedGoods = (FinishedGoods)store.getGoods();
        return finishedGoods;
    }

}// End of class

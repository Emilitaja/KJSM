/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author emil
 */
public class FinishedGoodsStore implements Store {

    @Override
    public Goods getGoods() {
        FinishedGoods finishedGoods = new FinishedGoods();
        return finishedGoods;
    }
}// End of class

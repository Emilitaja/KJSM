/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        ArrayList<String> la = new ArrayList<>(Arrays.asList("heyo1", "keyo2", "tejo3", "neljäs4", "ehheh5"));
        Toteutus a = new Toteutus(la ,new YksiListaus());
        a.printThis();
        Toteutus b = new Toteutus(la ,new KaksiListaus());
        b.printThis();
        Toteutus c = new Toteutus(la ,new KolmeListaus());
        c.printThis();
    }
}

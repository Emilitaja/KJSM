/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author emil
 */
class Peli {
    
    Pelaaja pelaaja;
    int arvio;
    Arvottava arvo;
    
    public Peli() {
    }
    
    void liityPeliin(Pelaaja pelaaja) {
        this.pelaaja = pelaaja;
        pelaaja.setObj(arvo);
        arvio = pelaaja.arvaa();
        if (arvo.getArvo() == arvio){
            peliloppu(pelaaja);
        }else{
            liityPeliin(pelaaja);
        }
    }
    
    public void setArvo(Arvottava arvo) {
        this.arvo = arvo;
    }

    private void peliloppu(Pelaaja pelaaja) {
        System.out.println(pelaaja.nimi + " arvasi oikein");
    }
    /*public void liityPeliin(Pelaaja pelaaja){
        Arvottava arvo = new Arvottava(arvoluku());
        pelaaja.arvo = arvo;
    }

    private int arvoluku() {
        return 3;
    }*/
}

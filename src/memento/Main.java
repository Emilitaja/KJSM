/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author emil
 */
public class Main {
    public static void main(String[] args){
        Pelaaja pelaaja = new Pelaaja();
        pelaaja.setNimi("pekka");
        Peli peli = new Peli();
        peli.setArvo(new Arvottava(3));
        peli.liityPeliin(pelaaja);
    }
}

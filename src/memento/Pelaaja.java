/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Scanner;

/**
 *
 * @author emil
 */
class Pelaaja {
    String nimi;
    int arvaus;
    Object obj;
    Scanner sc = new Scanner(System.in);
    Object arvo;

    public Pelaaja() {
    }

    public Pelaaja(String nimi) {
        this.nimi = nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setArvaus(int arvaus) {
        this.arvaus = arvaus;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    public int arvaa() {
        System.out.println("arvaa luku! :");
        arvaus = sc.nextInt();
        return arvaus;
    }
    
    
}

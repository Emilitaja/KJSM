/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author emil
 */
class Arvottava {
    int arvo;

    public Arvottava(int arvo) {
        this.arvo = arvo;
    }

    public Arvottava() {
    }

    public void setArvo(int arvo) {
        this.arvo = arvo;
    }

    public int getArvo() {
        return arvo;
    }

    public Object save() {
        return new Arvottava(this.arvo);
    }
    
}
